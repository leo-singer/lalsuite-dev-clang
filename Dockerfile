FROM containers.ligo.org/docker/lalsuite-dev:stretch

LABEL name="LALSuite Development - Clang 7" \
      maintainer="Adam Mercer <adam.mercer@ligo.org>" \
      support="Best Effort"

# import required key
RUN wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# add llvm repository
RUN echo "deb http://apt.llvm.org/stretch/ llvm-toolchain-stretch-7 main" > /etc/apt/sources.list.d/llvm.list

# install clang
RUN apt-get update && apt-get --assume-yes install clang-7

# clear package cache
RUN rm -rf /var/lib/apt/lists/*
